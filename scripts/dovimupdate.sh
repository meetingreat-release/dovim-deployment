LOG=/tmp/dovim_update.log
if [ ! -e ${LOG} ]; then
  echo First update started. > ${LOG}
fi
echo ____________________________________________ >> ${LOG}
echo dovim update at `date` >> ${LOG}
cd /srv/webapps/dovim
OLDVER="`head -n 1 version`"
git pull >> ${LOG}
NEWVER=`head -n 1 version`
echo Upgrade from version ${OLDVER} to ${NEWVER} >> ${LOG}
if [ $NEWVER -gt $OLDVER ]; then
  echo Executing post-upgrade shell ... >> ${LOG}
  echo . scripts/post-upgrade.sh $OLDVER $NEWVER >> ${LOG}
fi
echo Upgrade Done. >> ${LOG}
