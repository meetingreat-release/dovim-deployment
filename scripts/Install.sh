#!/bin/bash

echo Installing packages ...
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo pip install python-memcached
echo Packages installed.
echo

echo Setup environments ...
chmod +x dovim ldapGetUser
mkdir -p web/dovim/clients/windows
mkdir -p web/dovim/servers
mkdir -p uploads
chmod 777 uploads
cp sample-config/auth_by_email.conf dovim.conf
echo

# generate server key
echo "Generating server key ..."
ssh-keygen -f server.key -t rsa -N ''
ssh-keygen -f server.key.pub -e -m pem > pubkey
mv pubkey server.key.pub
# create web/dovim/servers/this for public key
FINGERPRINT=`../fingerprint server.key.pub`
PUBKEY=`awk '{ printf "    \"%s\",\n", $0 }' server.key.pub`
cat > web/dovim/servers/this <<EOF
{
  "version" : 2,
  "port": 8989,
  "auth": "email",
  "fingerprint": "${FINGERPRINT}",
  "pubkey": [
${PUBKEY}
    "" ]
}
EOF


# generate license file
echo Generating license file ...
CHOICES=(100 200 500)
while :
do
  for i in "${!CHOICES[@]}"; do 
    printf "(%d) %s Users\n" "$((i+1))" "${CHOICES[$i]}"
  done
  read -p "Please select user license: " -n1 -s
  echo
  case "$REPLY" in
    [123] )
      USER=${CHOICES[$((REPLY-1))]}
      read -p "You chose $USER Users. Correct?(Y/N) " -n 1 -r
      echo
      if [[ $REPLY =~ ^[Yy]$ ]]; then
        break
      fi
      ;;

     * ) echo "Invalid option";;
    esac
done

cat > license.txt <<EOF
{
  "Product": "Meetingreat Dovim Server",
  "Users": ${USER},
  "RoomAudio": true,
  "RoomVideo": false
}
EOF
../licgen -aes license.txt license
rm license.txt license.verify
echo

# add dovim user in cvc
(cd /srv/webapps/mgcvc; sudo -u www-data ./venv/bin/python manage.py setuser -n dovim -p meetingreat -a 1 -e 1)

git config --local user.email "dovim@localhost"
git config --local user.name DovimAutoUpdate

# append crontab
echo "Appending root's crontab ..."
UPDATECMD=/srv/webapps/dovim/scripts/autoupdate.sh
if [ -z "`sudo crontab -l | grep ${UPDATECMD}`" ]; then
  (sudo crontab -l 2>/dev/null; echo '# dovim update on 3:00am everyday'; echo "0 3 * * * ${UPDATECMD}") | sudo crontab -
fi

# echo Patch the system  ...
# sudo chown www-data.www-data server.key
sudo cp etc/init/dovim.conf /etc/init
sudo cp etc/nginx/sites-available/dovim.locations /etc/nginx/sites-available
sudo cp /etc/nginx/sites-available/mgcvc.https /etc/nginx/sites-available/mgcvc.https.org
cat etc/nginx/sites-available/mgcvc.https.diff | sudo patch -f -p1 -r /tmp/mgcvc.https.rej --no-backup-if-mismatch /etc/nginx/sites-available/mgcvc.https
(cd /etc/nginx/sites-enabled ; sudo ln -s ../sites-available/mgcvc.tohttps ; sudo rm mgcvc)
#if [ -z "`grep dovim.locations /etc/nginx/sites-available/mgcvc.https`" ]; then
#  sudo sed -i '/include sites-available\/mgcvc.base/a \
#    include sites-available\/dovim.locations\;' /etc/nginx/sites-available/mgcvc.https
#fi
sudo service nginx restart

echo Done.
