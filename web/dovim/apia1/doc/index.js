$(document).ready(function() {
	$.ajaxSetup({ cache: false });

	dovimAppId = "abcdefg";
	dovimApiKey = "1234567890";

	// query users
	function dovimUsersGet() {
		return $.ajax({
			url: "/dovim/apia1/users",
			method: "GET",
			dataType: "json",
			headers: {
				"X-Dovim-App-Id": dovimAppId,
				"X-Dovim-API-Key": dovimApiKey,
			},
			data: {
			}
		})
		.done(function(data, textStatus, jqXHR) {
			// console.log(data);
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			console.log(jqXHR, textStatus, errorThrown);
		});
	}

	// add user
	function dovimUsersPost(user) {
		return $.ajax({
			url: "/dovim/apia1/users",
			method: "POST",
			dataType: "json",
			contentType: "application/json; charset=utf-8",
			headers: {
				"X-Dovim-App-Id": dovimAppId,
				"X-Dovim-API-Key": dovimApiKey,
			},
			data: JSON.stringify(user),
		})
		.done(function(data, textStatus, jqXHR) {
			// console.log(data);
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			console.log(jqXHR, textStatus, errorThrown);
		});
	}

	// query user by userId
	function dovimUserGet(userId) {
		return $.ajax({
			url: "/dovim/apia1/users/" + userId.toString(),
			method: "GET",
			dataType: "json",
			headers: {
				"X-Dovim-App-Id": dovimAppId,
				"X-Dovim-API-Key": dovimApiKey,
			},
		})
		.done(function(data, textStatus, jqXHR) {
			// console.log(data);
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			console.log(jqXHR, textStatus, errorThrown);
		});
	}

	// update user
	function dovimUserPut(user) {
		return $.ajax({
			url: "/dovim/apia1/users/" + user.Id.toString(),
			method: "PUT",
			dataType: "json",
			contentType: "application/json; charset=utf-8",
			headers: {
				"X-Dovim-App-Id": dovimAppId,
				"X-Dovim-API-Key": dovimApiKey,
			},
			data: JSON.stringify(user),
		})
		.done(function(data, textStatus, jqXHR) {
			// console.log(data);
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			console.log(jqXHR, textStatus, errorThrown);
		});
	}

	// delete user
	function dovimUserDelete(userId) {
		return $.ajax({
			url: "/dovim/apia1/users/" + userId.toString(),
			method: "DELETE",
			dataType: "json",
			headers: {
				"X-Dovim-App-Id": dovimAppId,
				"X-Dovim-API-Key": dovimApiKey,
			},
		})
		.done(function(data, textStatus, jqXHR) {
			// console.log(data);
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			console.log(jqXHR, textStatus, errorThrown);
		});
	}

	function test_delete_user(userId) {
		// delete user
		dovimUserDelete(userId).done(function() {
			console.log("Delete user ok");

			// show users again
			dovimUsersGet().done(function(users) {
				console.log("Now all users:", users);
			});
		})
	}

	function test_update_user(user) {
		// modify user data
		user.First_Name = "bar";
		user.Last_Name = "foo";
		user.Username = "BarFoo";
		user.Email = "barfoo@avamagic.com"
		dovimUserPut(user).done(function() {
			console.log("Update user ok");
			var userId = user.Id;

			// query updated user
			dovimUserGet(userId).done(function(user) {
				console.log("Updated user:", user);

				// test delete
				test_delete_user(userId);
			});
		});
	}

	function test_get_user(userId) {
		// get new user by userId
		dovimUserGet(userId).done(function(user) {
			console.log("New user:", user);
			var userCloned = $.extend({}, user); // clone for log
			test_update_user(userCloned);
		});
	}

	function test_add_user() {
		// add new user
		dovimUsersPost({
			First_Name: "Foo",
			Last_Name: "Bar",
			Username: "FooBar",
			Phone: "123456789",
			Email: "FooBar@meetingreat.com",
		}).done(function(data) {
			var userId = data.Id;
			console.log("New userId:", userId);
			if (userId > 0) {
				test_get_user(userId);
			}
		});
	}

	function test_main() {
		// dovimUserDelete(1005);
		// get all users
		dovimUsersGet().done(function(users) {
			console.log("All users:", users);
			test_add_user();
		});
	}

	// test_main(); // run test main
});
