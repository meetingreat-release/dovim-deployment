# -*-coding:utf8-*-

import sys
import os
import memcache
import requests
import logging
import base64
from hashlib import md5

try:
    import simplejson as json
except ImportError:
    try:
        import json
    except ImportError:
        try:
            from django.utils import simplejson as json
        except ImportError:
            raise ImportError('A json library is required to use this python library')

# logging.basicConfig(format='%(levelname)-8s [%(asctime)s]  %(message)s', level=logging.DEBUG)


class PySendPulse:
    """ SendPulse REST API python wrapper
    """
    __api_url = "https://api.sendpulse.com"
    __user_id = None
    __secret = None
    __token = None
    __token_file_path = ""
    __token_hash_name = None
    __storage_type = "FILE"
    __refresh_token = 0

    MEMCACHED_VALUE_TIMEOUT = 3600
    ALLOWED_STORAGE_TYPES = ['FILE', 'MEMCACHED']

    def __init__(self, user_id, secret, storage_type="FILE"):
        """ SendPulse API constructor

        @param user_id: string REST API ID from SendPulse settings
        @param secret: string REST API Secret from SendPulse settings
        @param storage_type: string FILE|MEMCACHED
        @raise: Exception empty credentials or get token failed
        """
        logging.info("Initialization SendPulse REST API Class")
        if not user_id or not secret:
            raise Exception("Empty ID or SECRET")

        self.__user_id = user_id
        self.__secret = secret
        self.__storage_type = storage_type.upper()
        m = md5()
        m.update("{}::{}".format(user_id, secret))
        self.__token_hash_name = m.hexdigest()
        if self.__storage_type not in self.ALLOWED_STORAGE_TYPES:
            logging.warn("Wrong storage type '{}'. Allowed storage types are: {}".format(storage_type, self.ALLOWED_STORAGE_TYPES))
            logging.warn("Try to use 'FILE' instead.")
            self.__storage_type = 'FILE'
        logging.debug("Try to get security token from '{}'".format(self.__storage_type, ))
        if self.__storage_type == "MEMCACHED":
            mc = memcache.Client(['127.0.0.1:11211'])
            self.__token = mc.get(self.__token_hash_name)
        else:  # file
            filepath = "{}{}".format(self.__token_file_path, self.__token_hash_name)
            if os.path.isfile(filepath):
                with open(filepath, 'rb') as f:
                    self.__token = f.readline()

            else:
                logging.error("Can't find file '{}' to read security token.".format(filepath))
        logging.debug("Got: '{}'".format(self.__token, ))
        if not self.__token and not self.__get_token():
            raise Exception("Could not connect to API. Please, check your ID and SECRET")

    def __get_token(self):
        """ Get new token from API server and store it in storage
        @return: boolean
        """
        logging.debug("Try to get new token from server")
        self.__refresh_token += 1
        data = {
            "grant_type": "client_credentials",
            "client_id": self.__user_id,
            "client_secret": self.__secret,
        }
        response = self.__send_request("oauth/access_token", "POST", data, False)
        if response.status_code != 200:
            return False
        self.__refresh_token = 0
        self.__token = response.json()['access_token']
        logging.debug("Got: '{}'".format(self.__token, ))
        if self.__storage_type == "MEMCACHED":
            logging.debug("Try to set token '{}' into 'MEMCACHED'".format(self.__token, ))
            mc = memcache.Client(['127.0.0.1:11211'])
            mc.set(self.__token_hash_name, self.__token, self.MEMCACHED_VALUE_TIMEOUT)
        else:
            filepath = "{}{}".format(self.__token_file_path, self.__token_hash_name)
            try:
                with open(filepath, 'w') as f:
                    f.write(self.__token)
                    logging.debug("Set token '{}' into 'FILE' '{}'".format(self.__token, filepath))
            except IOError:
                logging.warn("Can't create 'FILE' to store security token. Please, check your settings.")
        if self.__token:
            return True
        return False

    def __send_request(self, path, method="GET", params=None, use_token=True):
        """ Form and send request to API service

        @param path: sring what API url need to call
        @param method: HTTP method GET|POST|PUT|DELETE
        @param params: dict argument need to send to server
        @param use_token: boolean need to use token or not
        @return: HTTP requests library object http://www.python-requests.org/
        """
        url = "{}/{}".format(self.__api_url, path)
        method.upper()
        logging.debug("__send_request method: {} url: '{}' with parameters: {}".format(method, url, params))
        if type(params) not in (dict, list):
            params = {}
        if use_token and self.__token:
            headers = {'Authorization': 'Bearer {}'.format(self.__token)}
        else:
            headers = {}
        if method == "POST":
            response = requests.post(url, headers=headers, data=params)
        elif method == "PUT":
            response = requests.put(url, headers=headers, data=params)
        elif method == "DELETE":
            response = requests.delete(url, headers=headers, data=params)
        else:
            response = requests.get(url, headers=headers, params=params)
        if response.status_code == 401 and self.__refresh_token == 0:
            self.__get_token()
            return self.__send_request(path, method, params)
        elif response.status_code == 404:
            logging.warn("404: Sorry, the page you are looking for could not be found.")
            logging.debug("Raw_server_response: {}".format(response.text, ))
        elif response.status_code == 500:
            logging.critical("Whoops, looks like something went wrong on the server. Please contact with out support tech@sendpulse.com.")
        else:
            try:
                logging.debug("Request response: {}".format(response.json(), ))
            except:
                logging.critical("Raw server response: {}".format(response.text, ))
                return response.status_code
        return response

    def __handle_result(self, data):
        """ Process request results

        @param data:
        @return: dictionary with response message and/or http code
        """
        if 'status_code' not in data:
            if data.status_code == 200:
                logging.debug("Hanle result: {}".format(data.json(), ))
                return data.json()
            elif data.status_code == 404:
                response = {
                    'is_error': True,
                    'http_code': data.status_code,
                    'message': "Sorry, the page you are looking for {} could not be found.".format(data.url, )
                }
            elif data.status_code == 500:
                response = {
                    'is_error': True,
                    'http_code': data.status_code,
                    'message': "Whoops, looks like something went wrong on the server. Please contact with out support tech@sendpulse.com."
                }
            else:
                response = {
                    'is_error': True,
                    'http_code': data.status_code
                }
                response.update(data.json())
        else:
            response = {
                'is_error': True,
                'http_code': data
            }
        logging.debug("Hanle result: {}".format(response, ))
        return {'data': response}

    def __handle_error(self, custom_message=None):
        """ Process request errors

        @param custom_message:
        @return: dictionary with response custom error message and/or error code
        """
        message = {'is_error': True}
        if custom_message is not None:
            message['message'] = custom_message
        logging.error("Hanle error: {}".format(message, ))
        return message


    def smtp_send_mail(self, email):
        """ SMTP: send email

        @param email: string valid email address. We will send an email message to the specified email address with a verification link.
        @return: dictionary with response message
        """
        logging.info("Function call: smtp_send_mail")
        if not email.get('html') or not email.get('text'):
            return self.__handle_error('Seems we have empty body')
        elif not email.get('subject'):
            return self.__handle_error('Seems we have empty subject')
        elif not email.get('from') or not email.get('to'):
            return self.__handle_error("Seems we have empty some credentials 'from': '{}' or 'to': '{}' fields".format(email.get('from'), email.get('to')))
        email['html'] = base64.b64encode(email.get('html'))
        return self.__handle_result(self.__send_request('smtp/emails', 'POST', {'email': json.dumps(email)}))

if __name__ == "__main__":
    REST_API_ID = '73c1f3f9b6a7f9f12961be1ddbdc04ab'
    REST_API_SECRET = 'bee728b1ec51efa8e5f1b3a02a28f76c'
    TOKEN_STORAGE = 'memcached'
    

    if len(sys.argv) < 3:
        print 'Usage: ' + sys.argv[0] + ' <email-address> <code>'
        sys.exit()
        
      
    # Send mail using SMTP
    email = {
        'subject': 'Dovim authentication code',
        'html': '<h1>{}</h1><p>Please type the authentication code {} in your phone.</p>'.format(sys.argv[2], sys.argv[2]),
        'text': 'Please type the authentication code {} in your phone.'.format(sys.argv[2]),
        'from': {'name': 'Dovim Support', 'email': 'support@meetingreat.com'},
        'to': [
            { 'email': sys.argv[1] }
        ]
    }
    
    SPApiProxy = PySendPulse(REST_API_ID, REST_API_SECRET, TOKEN_STORAGE)
    SPApiProxy.smtp_send_mail(email)
